package com.example.spinerpersonalizado2;

public class ItemData {
    private String txtCategoria;
    private String txtDescripcion;
    private int idImagen;

    public ItemData(String txtCategoria, String txtDescripcion, int idImagen) {
        this.txtCategoria = txtCategoria;
        this.txtDescripcion = txtDescripcion;
        this.idImagen = idImagen;
    }

    public String getTxtCategoria() {
        return txtCategoria;
    }

    public String getTxtDescripcion() {
        return txtDescripcion;
    }

    public int getIdImagen() {
        return idImagen;
    }

    public void setTxtCategoria(String txtCategoria) {
        this.txtCategoria = txtCategoria;
    }

    public void setTxtDescripcion(String txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public void setIdImagen(int idImagen) {
        this.idImagen = idImagen;
    }
}
